<?php/**

**/?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">   
	<title>action box setup</title>
	<link rel="stylesheet" id="fonts-css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A400%2C300%2C300italic%2C400italic%2C600%2C600italic%2C700%2C700italic%2C900%2C900italic&amp;ver=1" type="text/css" media="all">
	<script src="https://use.fontawesome.com/17d65911f6.js"></script>
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        body, html {
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 300;
            font-style: normal;
            color: #262626;
            font-size: 16px;
            overflow-y: scroll !important;
            width: 100% !important;
            margin: 0;
        }
        body {
            padding: 20px;
        }
        input {
            display: block;
            font-family: 'Source Sans Pro', sans-serif;
            padding: 5px 5px;
            font-size: 16px;
            margin-bottom: 15px;
            width: 100%;
            font-weight: 300;
        }
        h1 {
            margin-top: 0;
        }
        label {
            color: #505050;
            font-size: 16px;
            font-weight: 400;
            margin-bottom: 5px;
        }
        button {
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 400;
            padding: 15px 20px;
            font-size: 16px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            line-height: 1;
            letter-spacing: 0;
            display: inline-block;
            border: none;
            text-transform: uppercase;
            color: #fff;
            -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            background-color: #36bd25;
            color: #fff;
            outline: none;
            text-decoration: none;
            cursor: pointer;
        }
        /* ========== CHECKBOXES ============ */
        .checkbox__input {
            position: absolute;
            left: -9999px;
            opacity: 0;
        }
        .checkbox__label {
            position: relative;
            padding-left: 25px;
            cursor: pointer;
            display: table-cell;
        }
        .checkbox {
            color: #505050;
            font-size: 16px;
            font-weight: 400;
            line-height: 18px;
        }
        .checkbox__label:before {
            content: "";
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: .24s;
            -o-transition: .24s;
            -moz-transition: .24s;
            transition: .24s;
            left: 0;
            border-width: 2px;
            border-style: solid;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;
            position: absolute;
            top: 50%;
            margin-top: -10px;
            height: 20px;
            width: 20px;
        }
        .checkbox__label--normal:before {
            border-color: rgba(0, 0, 0, 0.54);
        }
        .checkbox__label--blue:before {
            border-color: #00A0DB;
        }
        .checkbox__label:after {
            content: '';
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transform: rotate(45deg);
            -moz-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            -o-transform: rotate(45deg);
            transform: rotate(45deg);
            position: absolute;
            top: 2px;
            display: table;
            border-width: 2px;
            border-style: solid;
            border-top: 0;
            border-left: 0;
            border-color: rgba(255, 255, 255, 0.87);
            visibility: hidden;
            opacity: 0;
            -webkit-transition: opacity .28s ease;
            -o-transition: opacity .28s ease;
            -moz-transition: opacity .28s ease;
            transition: opacity .28s ease;
            left: 7px;
            width: 6.66667px;
            height: 13.33333px;
        }
        .checkbox [type="checkbox"]:checked + .checkbox__label:after {
            opacity: 1;
            top: 50%;
            visibility: visible;
            margin-top: -8px;
        }
        .checkbox [type="checkbox"]:checked + .checkbox__label--normal:before {
            border-color: transparent;
            background-color: rgba(2, 119, 191, 0.87);
        }
        .checkbox [type="checkbox"]:checked + .checkbox__label--blue:before {
            border-color: transparent;
            background-color: #00A0DB;
        }
    </style>
</head>
<body class="cinderella-template">
    <h1 id="title">Action Box settings</h1>
    <div>
        <label for="title">Enter a title for the action box</label>
        <input id="titleText" name="title" type="text" placeholder="enter title here">
        <label for="subtitle">Enter a subtitle for the action box</label>
        <input id="subtitleText" name="subtitle" type="text" placeholder="enter subtitle here">
        <label for="size">Enter the desired size of the action box (50 or 100)</label>
        <input id="size" name="size" type="text" placeholder="enter the desired size here">
        <label for="linkText">Enter the text for the link in the action box</label>
        <input id="linkText" name="linkText" type="text" placeholder="enter linktext here">
        <label for="linkUrl">Enter the URL to which the CTA should point eg: /signup/</label>
        <input id="linkUrl" name="linkUrl" type="text" placeholder="enter the url for the link here">
        <div style="margin: 20px 0;">
            <label class="checkbox">
                <input id="openLinkOutside" type="checkbox" value="yes" class="checkbox__input">
                <span class="checkbox__label checkbox__label--blue">Open the link in a new tab</span>
            </label>
        </div>
    </div>
    <div><button type="button" id="saveData">Save data</button>
<script type="text/javascript">
    var sourceWindow, sourceOrigin;
    window.addEventListener('message', function(event) {
        sourceWindow = event.source
        sourceOrigin = event.origin
        event.source.postMessage('Hola bert', event.origin)
    })

    function getInputValues() {
        var titleText = document.getElementById('titleText').value;
        var subtitleText = document.getElementById('subtitleText').value;
        var size = document.getElementById('size').value;
        var linkText = document.getElementById('linkText').value;
        var linkUrl = document.getElementById('linkUrl').value;
        var linkDirection = document.getElementById('openLinkOutside').checked;

        return {
            title: titleText,
            subtitle: subtitleText,
            size: size,
            linkText: linkText,
            linkUrl: linkUrl,
            linkDirection: linkDirection
        }
    }

    document.getElementById('saveData').addEventListener('click', function() {
        sourceWindow.postMessage(getInputValues(), sourceOrigin);
        setTimeout(() => {
            document.getElementById('titleText').value = "";
            document.getElementById('subtitleText').value = "";
            document.getElementById('size').value = "";
            document.getElementById('linkText').value = "";
            document.getElementById('linkUrl').value = "";
        }, 100);
    })

</script>
</body>
</html>